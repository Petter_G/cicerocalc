import React, { Component } from 'react';

import {styles} from './loanCalculationComponentStyle'

//our decidedly most important component. Represents pretty much the entire program.
class LoanCalculationComponent extends Component
{
    constructor(props)
    {
        super(props);

        this.state =  {
            result: null,

            loanRaisingMonth: "",
            loanRaisingYear: "",
            principalAmount: "",
            annualNominalInterestRate: "",
            totalNumberOfPayments:"",
        }

        this.doQuery = this.doQuery.bind(this);
        this.processResponse = this.processResponse.bind(this);
    }

    processResponse( response )
    {
        //Now, according to the assignment, the first element of this arrays payment field is the value we want shown, but I suspect that's a mistake. The very first value is the amortized payment, plus 1500.
        // As far as I can tell, that 1500 is a onetime fee, as the value is constant no matter the duration or size of the loan.
        //Therefore, I am going to display the payment of element 0 minus 1500.

        var data = JSON.parse(response )
        var array = data.amortizationSchedule;

        var payment = array[0].payment - 1500;

        this.setState({
            result: "Your monthly repayment will be: " + payment +".  In addition, 1500 crowns will come as an additional fee on the very first repayment."

        })
    }

    doQuery()
    {
        //arguably one should've make rudimentary input validations here, no point bothering the server with obvious nonsense, like non-integers in our case. Time is short, so we're omitting it.
        var xhr = new XMLHttpRequest()
        xhr.onreadystatechange = () =>
        {
            //readyState can be one of five values: 0,1,2,3,4 meaning unsent,opened,headers received,loading and done respectively. We only care about done for now.
            if (xhr.readyState === XMLHttpRequest.DONE)//called when the response has been received.
            {
                if ( xhr.status === 200)//I.E ok
                {
                    this.processResponse( xhr.responseText )
                }
                else if ( xhr.status === 400)//something went wrong
                {
                    console.log("ERROR: status code is: " +xhr.status )
                }
                else
                {
                    console.log("ERROR: status code is: " +xhr.status )
                }
            }
        }

        var queryString = "https://services-test.cicero.no/cfp/7/cicero/rest/calculator/calculateLoan" +
            "?loanRaisingMonth=" + this.state.loanRaisingMonth +
            "&loanRaisingYear=" + this.state.loanRaisingYear +
            "&principalAmount=" + this.state.principalAmount +
            "&annualNominalInterestRate=" + this.state.annualNominalInterestRate +
            "&totalNumberOfPayments=" + this.state.totalNumberOfPayments;

        this.setState({
            loanRaisingMonth: "",
            loanRaisingYear: "",
            principalAmount: "",
            annualNominalInterestRate: "",
            totalNumberOfPayments:"",
        })

        //Note: To my surprise, submitting nothing in these fields is seen as valid input by the backend
        console.log("sending: "+ queryString)
        xhr.open('GET',queryString);
        xhr.send();
    }

    render()
    {
        return(
        <div>

            <div style={styles.gridContainer} >
                <div style={styles.welcome}>
                    <p style={styles.welcome.welcomeText}>Welcome to Cicero's loan calculator</p>
                    <p>Please input requested information</p>
                </div>

                {/*As a note, I wanted a fancier solution to the particular problem of input validation. Basically I only wanted to allow numeric input here, but getting it to work flawlessly would be very time consuming unless some built in solution could be
                found. Such functionality probably exists, this usecase is common with input fields, but alas I was unable to find one in time. Will have to just show an error message instead*/}
                <input style={styles.loanSum} value={this.state.principalAmount} placeholder="Loan sum:" onChange={ (evt) => this.setState({principalAmount: evt.target.value}) }/>
                <input style={styles.loanYears} value={this.state.totalNumberOfPayments} placeholder="Repayment period in months:" onChange={ (evt) => this.setState({totalNumberOfPayments: evt.target.value}) }/>
                <input style={styles.loanInterestRate} value={ this.state.annualNominalInterestRate} placeholder="Interest rate:" onChange={ (evt) => this.setState({annualNominalInterestRate: evt.target.value}) }/>
                <input style={styles.loanStartYear} value={ this.state.loanRaisingYear} placeholder="Starting year:" onChange={ (evt) => this.setState({loanRaisingYear: evt.target.value}) }/>
                <input style={styles.loanStartMonth} value={ this.state.loanRaisingMonth} placeholder="Starting month:" onChange={ (evt) => this.setState({loanRaisingMonth: evt.target.value}) }/>

                <div style={styles.submitButtonDiv}>
                    <button onClick={ () => this.doQuery() } >Submit</button>
                </div>
                <div style={styles.resultArea}>
                    <p>{this.state.result}</p>
                </div>
            </div>

        </div>
        );
    }
}

export default LoanCalculationComponent;
