import React from 'react';
import {Component} from "react";
import LoanCalculationComponent from "./loanCalculationComponent";

//exists solely to imitate modular design. In our app, the loanCalculationComponent is arguably the main view itself. In actual code though, such a component would typically be a part of a larger component, often called a view .
//makes it comparatively easy to add new components later on, without worrying about the existing ones.
export default class MainView extends Component
{
    render()
    {
        return(
            <LoanCalculationComponent/>
        )
    }
}