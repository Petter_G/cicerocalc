import React from 'react';

export const styles = {
    gridContainer: {
        borderStyle: "inset",
        borderRadius: "25px",
        backgroundColor: "#ADACAC",
        fontFamily: "Times New Roman",

        display: 'grid',
        gridTemplateColumns: "2fr 2fr 2fr 2fr",
        gridRowGap: "5px",
        gridColumnGap: "20px",
        gridTemplateAreas: "'... welcome welcome ...'"
                            + "'... loanSum loanSum ...'"
                            + "'... loanYears loanInterestRate ...'"
                            + "'... loanStartYear loanStartMonth ...'"
                            + "'... submitButtonDiv submitButtonDiv ...'"
                            + "'... resultArea resultArea ...'"
    },


    welcome: {
        gridArea: "welcome",
        welcomeText: {
            fontSize: "24px"
        }
    },

    loanSum: {
        gridArea: "loanSum",
    },

    loanYears: {
        gridArea: "loanYears",
    },

    loanStartYear:{
        gridArea: "loanStartYear"
    },

    loanStartMonth: {
        gridArea: "loanStartMonth"
    },

    loanInterestRate: {
        gridArea: "loanInterestRate",
    },

    submitButtonDiv: {
        gridArea: "submitButtonDiv",
        textAlign: "center",
        margin: "10px"
    },

    resultArea: {
        backgroundColor: "white",
        borderRadius: "5px",

        height: "50px",
        gridArea: "resultArea",
        textAlign: "center",
    },
}
