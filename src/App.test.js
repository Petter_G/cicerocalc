import React from 'react';
import ReactDOM from 'react-dom';
import LoanCalculationComponent from './mainView/loanCalculationComponent';

it('renders without crashing', () =>
{
  const div = document.createElement('div');
  ReactDOM.render(<LoanCalculationComponent />, div);
  ReactDOM.unmountComponentAtNode(div);
});
