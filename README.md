***This project was created in relation to a job application at Cicero Consulting***

**Note**

Since the task was given in English, this project was also conducted in English, even though I am pretty sure anyone who will read this speak fluent Norwegian.

**Installation instructions**

	1. Install Node and git if needed, I used version 8.11.1 of Node, which is the LTS version. The Node package can be found here: https://nodejs.org/en/download/, here for Git: https://git-scm.com/downloads
	2. Next you need to pull the project. Open a terminal, navigate to your preferred scratch folder, and run "git clone https://Petter_G@bitbucket.org/Petter_G/cicerocalc.git", this will probably take a while.
	3. Now you need to install the dependecies, navigate to where you pulled the repository, and run "npm install", this will also take a while.
	4. You're almost ready, run "npm start". Might also take a while, depending on your hardware.

A browser should open automatically, connecting to your server.

**Usage**
Usage is pretty self explainatory. Enter the requested information, and press submit.

**Known limitations**

	1. No frontend side input validation. You can enter whatever you like, the backend will however validate, and deny access if your form is found wanting.
	2. Similarily, the user isn't given any feedback when input is invalidated.

**Functionality**
It's a very simple program. All it does it query the Cicero CFS service for mortgage data using user supplied form data, and show the amortized repayment cost. That's all.

**Design decisions**
I went for a simple design. I originally envisioned using material-ui, which I've used in the past. However I decided against it when I realiced I was shorter on time than anticipated, experience with that library thought me that development time increases dramatically. I also intended to use MomentJS to get the current year and month, however I figured that would just reduce the usability, presumably a customer might want to calculate mortgage costs at a different date than the current one.